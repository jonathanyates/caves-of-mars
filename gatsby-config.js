module.exports = {
  siteMetadata: {
    title: `Caves Of Mars - Action sci-fi space shooter video game`,
    description: `Caves of Mars is an action sci-fi space shooter video game set in the mysterious caves of the planet mars available on Steam, iOS and Android`,
    siteUrl: `https://www.cavesofmars.com/`,
    author: `Silkweb Games`,
    twitterUsername: `@SilkwebG`,
    keywords: `caves of mars, mars, video game, space shooter, sci-fi, space, platform game`,
    image: `src/images/caves_of_mars_logo.png`
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds: [
          "G-1H1Y7KXKYR", // Google Analytics / GA
        ],
        // This object is used for configuration specific to this plugin
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: true,
        },
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-sitemap`,
    'gatsby-plugin-robots-txt',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Caves Of Mars`,
        short_name: `Caves Of Mars`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/player_spacecraft/NormalShip.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-gatsby-cloud`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
