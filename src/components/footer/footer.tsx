import React from 'react';
import { Container } from 'react-bootstrap';
import Copyright from '../common/Copyright';
import SocialMedia from '../common/socialMedia';
import './footer.scss';

export const Footer = () => (
  <footer>
    <Container>
      <SocialMedia />
      <Copyright />
    </Container>
  </footer>
);
