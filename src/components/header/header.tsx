import { Link } from 'gatsby';
import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { socialMediaUrls } from '../common/socialMedia';
import './header.scss';

const Header = () => (
  <Navbar expand="lg" fixed="top" variant="dark">
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav" className="justify-content-center">
      <Nav variant="pills" defaultActiveKey="/home">
        <Link className="nav-link" to="/">
          Home
        </Link>
        <Link className="nav-link" to="/about">
          About
        </Link>
        <Link className="nav-link" to="/download">
          Download
        </Link>
        <a href={socialMediaUrls.youTube} target="_blank" rel="noreferrer" className="nav-link">
          Video
        </a>
        <Link className="nav-link" to="/images">
          Images
        </Link>

        <Link className="nav-link" to="/presskit">
          Presskit
        </Link>

        <Link className="nav-link" to="/contact">
          Contact
        </Link>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

export default Header;
