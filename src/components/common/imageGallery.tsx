import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { PhotoGallery } from './photoGallery';

const ImageGallary = () => (
  <>
    <Row className="justify-content-center">
      <Col className="d-flex justify-content-center" lg={10} md={8} sm={10} xs={10}>
        <PhotoGallery />
      </Col>
    </Row>
  </>
);

export default ImageGallary;
