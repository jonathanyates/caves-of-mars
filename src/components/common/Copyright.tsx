import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';
import { Row, Col } from 'react-bootstrap';

const Copyright = () => (
  <>
    <Row className="justify-content-center text-center mt-4">
      <Col>
        <a className="text-decoration-none text-reset" href="https://www.cavesofmars.com">
          <h5>
            <span>
              © {new Date().getFullYear()}
              {` `}
            </span>
            Caves Of Mars <br /> All Rights Reserved
          </h5>
        </a>
        <StaticImage className="mt-1" src="../../images/silkweb-logo.png" alt="Enemies" placeholder="none" />
      </Col>
    </Row>
  </>
);

export default Copyright;
