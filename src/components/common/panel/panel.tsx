import React, { ReactNode } from 'react';
import './panel.scss';

interface PanelProps {
  title: string;
  children: ReactNode;
}

const Panel = ({ title, children }: PanelProps) => (
  <div className="panel">
    <div className="panel-header text-center">
      <div className="panel-header-text">{title}</div>
    </div>

    <div className="panel-content">{children}</div>
  </div>
);

export default Panel;
