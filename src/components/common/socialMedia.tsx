import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { FaTwitter, FaFacebookSquare, FaInstagram, FaYoutube, FaEnvelope, FaSteam } from 'react-icons/fa';
import { IconLink } from './iconLink';
import './socialMedia.scss';

export const socialMediaUrls = {
  twitter: 'https://twitter.com/SilkwebG',
  steam: 'https://store.steampowered.com/app/1724180/Caves_Of_Mars/',
  instagram: 'https://www.instagram.com/silkwebgames',
  youTube: 'https://www.youtube.com/channel/UCMZsKrUHWVZHD3RoJ4DCbWQ',
  youTubeEmbedId: 'UDsnHQo6Il0',
  faceBook: 'https://www.facebook.com/silkwebgames',
  email: 'mailto:cavesofmars@silkwebgames.com',
  website: 'https://www.cavesofmars.com',
  playstore: 'https://play.google.com/store/apps/details?id=com.SilkwebCommerceLtd.CavesOfMars',
  appstore: 'https://apps.apple.com/us/app/caves-of-mars/id1581858744',
};

const SocialMedia = () => (
  <>
    <Row>
      <Col className="d-flex justify-content-center">
        <IconLink url={socialMediaUrls.twitter} icon={FaTwitter} text="Twitter" />
        <IconLink url={socialMediaUrls.instagram} icon={FaInstagram} text="Instagram" />
        <IconLink url={socialMediaUrls.steam} icon={FaSteam} text="Steam" />
      </Col>
    </Row>

    <Row>
      <Col className="d-flex justify-content-center">
        <IconLink url={socialMediaUrls.youTube} icon={FaYoutube} text="YouTube" />
        <IconLink url={socialMediaUrls.faceBook} icon={FaFacebookSquare} text="Facebook" />
        <IconLink url={socialMediaUrls.email} icon={FaEnvelope} text="Email" />
      </Col>
    </Row>
  </>
);

export default SocialMedia;
