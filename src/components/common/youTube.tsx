import React from 'react';

interface YouTubeProps {
  title: string;
  embedId: string;
}

export const YouTube = ({ title, embedId }: YouTubeProps) => (
  <div className="video-responsive">
    <iframe
      width="100%"
      height="100%"
      src={`https://www.youtube.com/embed/${embedId}`}
      title={title}
      frameBorder="0"
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen={true}
    />
  </div>
);
