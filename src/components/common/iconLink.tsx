import React from 'react';
import { IconType } from 'react-icons';

interface IconLinkProps {
  url: string;
  icon: IconType;
  text: string;
}

export const IconLink = ({ url, icon, text }: IconLinkProps) => {
  var Icon = icon;
  return (
    <a href={url} target="_blank" rel="me noreferrer" className="social text-decoration-none text-reset mx-2">
      <h3 className="d-flex">
        <Icon />
        <span className="d-none d-sm-block mx-2">{text}</span>
      </h3>
    </a>
  );
};

interface MediaIconLinkProps {
  url: string;
  icon: IconType;
  text: string;
  smallText: string;
}

export const MediaIconLink = ({ url, icon, text, smallText }: MediaIconLinkProps) => {
  var Icon = icon;
  return (
    <a href={url} target="_blank" rel="me noreferrer" className="social text-decoration-none text-reset mx-2">
      <div className="d-flex">
        <Icon />
        <span className="mx-2 d-none d-lg-block">{text}</span>
        <span className="mx-2 d-lg-none">{smallText}</span>
      </div>
    </a>
  );
};
