import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Panel from './panel/panel';
import SocialMedia, { socialMediaUrls } from './socialMedia';

const DownloadPanel = () => (
  <Panel title="Available Now">
    <div className="text-center">
      <h2 className="my-4">The new action space shooter game set in the mysterious caves of planet Mars.</h2>
      <h1>Download on</h1>
      <h1> Steam, iOS and Android</h1>

      <Row className="justify-content-center">
        <Col xs={10} sm={10} md={10} lg={8} xl={6}>
          <div className="app-store-images-container">
            <a className="steam-image" href={socialMediaUrls.steam} target="_blank" rel="noreferrer">
              <StaticImage src="../../images/app_store/SteamLogo.png" alt="Play it on Steam" placeholder="none" />
            </a>
          </div>
          <div className="app-store-images-container">
            <a className="app-store-image" href={socialMediaUrls.appstore} target="_blank" rel="noreferrer">
              <StaticImage src="../../images/app_store/AppStore.png" alt="Download in the Apple App Store" placeholder="none" />
            </a>
            <a className="google-play-image" href={socialMediaUrls.playstore} target="_blank" rel="noreferrer">
              <StaticImage src="../../images/app_store/GooglePlay.png" alt="Get it on Google Play" placeholder="none" />
            </a>
          </div>
        </Col>
      </Row>

      <h3 className="mt-3 mb-3">Follow us on</h3>
      <SocialMedia />
    </div>
  </Panel>
);

export default DownloadPanel;
