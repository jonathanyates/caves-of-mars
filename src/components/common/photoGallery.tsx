// https://peterpalau.github.io/react-bnb-gallery/#/home

import 'react-bnb-gallery/dist/style.css';
import React from 'react';
import { useMemo } from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import { useState } from 'react';
import ReactBnbGallery, { Photo } from 'react-bnb-gallery';
import { useLocation } from '@reach/router';
import { Container } from 'react-bootstrap';
import { GatsbyImage, getImage, IGatsbyImageData } from 'gatsby-plugin-image';
import './photoGallery.scss';
interface PhotoGalleryState {
  isOpen: boolean;
  activePhotoIndex: number;
}

interface ImageNode {
  publicURL: string;
  name: string;
}

export const PhotoGallery = () => {
  const [state, setState] = useState<PhotoGalleryState>({ isOpen: false, activePhotoIndex: 0 });
  const { origin } = useLocation();

  const data: any = useStaticQuery(graphql`
    {
      allFile(filter: { relativeDirectory: { eq: "screenshots" } }) {
        nodes {
          publicURL
          name
          childImageSharp {
            gatsbyImageData(placeholder: BLURRED, formats: [AUTO, WEBP, AVIF])
          }
        }
      }
    }
  `);

  const getPath = (node: ImageNode) => `${origin}${node.publicURL}`;

  var nodes = useMemo(() => data.allFile.nodes.sort((a: ImageNode, b: ImageNode) => a.name.localeCompare(b.name)), [
    data.allFile.nodes,
  ]);

  var totalPhotos = nodes.length;

  var photos: Photo[] = nodes.map((node: ImageNode, i: number) => ({
    photo: getPath(node),
    number: i + 1,
    caption: `${i + 1} of ${totalPhotos}`, // : ${node.name}
  }));

  return (
    <Container>
      <div className="photo-gallary-grid">
        {nodes.slice(0, 12).map((node: any, i: number) => {
          var image = getImage(node) as IGatsbyImageData;
          return (
            <div onClick={() => setState({ isOpen: true, activePhotoIndex: i })}>
              <GatsbyImage image={image} alt={node.name} />
            </div>
          );
        })}

        <ReactBnbGallery
          show={state.isOpen}
          activePhotoIndex={state.activePhotoIndex}
          photos={photos}
          backgroundColor="#140000"
          onClose={() => setState({ isOpen: false, activePhotoIndex: 0 })}
        />
      </div>

      <button
        onClick={() => setState({ isOpen: true, activePhotoIndex: 0 })}
        className="nav-link blue-button text-decoration-none text-reset mt-4"
      >
        more..
      </button>
    </Container>
  );
};
