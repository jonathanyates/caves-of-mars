import React from 'react';
import { Row } from 'react-bootstrap';

interface EmptyRowProps {
  size?: number;
}

export const EmptyRow = ({ size = 2 }: EmptyRowProps) => <Row className={`my-${size}`} />;
