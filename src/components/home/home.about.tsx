import { Link } from 'gatsby';
import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Panel from '../common/panel/panel';
import './home.scss';

export const HomeAbout = () => (
  <>
    <Row>
      <Col>
        <Panel title="About">
          <div>
            <div>
              <p>The Caves Of Mars is the new 2D action space shooter game set in the mysterious caves of the planet Mars.</p>
              <p>
                Navigate your spacecraft through these caves, destroy enemies and alien lifeforms and collect as many gems as
                possible. Collecting gems will increase your health and gain points. You will gain extra bonus points the more
                gems you collect and enemies you destroy.
              </p>
              <p>
                There are 100 cave levels to complete, each one procedurally generated randomly so no two levels are ever the
                same. As you progress through the levels the caves will become larger, more complex and you will encounter more
                challenging enemies, aliens and obstacles.
              </p>
            </div>
            <Link to="/about" className="nav-link blue-button text-decoration-none text-reset">
              more..
            </Link>
          </div>
        </Panel>
      </Col>
    </Row>
  </>
);
