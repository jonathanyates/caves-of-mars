// import { Link } from 'gatsby';
import { StaticImage } from 'gatsby-plugin-image';
import React, { useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import Panel from '../common/panel/panel';
import { socialMediaUrls } from '../common/socialMedia';
import { YouTube } from '../common/youTube';
import './home.trailer.scss';

export const HomeTrailer = () => {
  const [clickedState, setClickedState] = useState(false);

  return (
    <Panel title="Trailer">
      <Row className="justify-content-center my-4">
        <Col xs={10}>
          {clickedState ? (
            <YouTube embedId={socialMediaUrls.youTubeEmbedId} title="Caves Of Mars Trailer"></YouTube>
          ) : (
            <div className="position-relative" onClick={() => setClickedState(true)}>
              <StaticImage
                src="../../images/screenshots/Screenshot 2021-07-17 at 14.44.43.jpg"
                alt="Trailer"
                placeholder="none"
                layout="fullWidth"
              />
              <div className="youtube-play-wrapper">
                <div className="youtube-play">
                  <StaticImage src="../../images/youTubePlay.png" alt="Trailer" placeholder="none" />
                </div>
              </div>
            </div>
          )}
        </Col>
      </Row>

      {/* <a
        href={socialMediaUrls.youTube}
        target="_blank"
        rel="noreferrer"
        className="nav-link blue-button text-decoration-none text-reset"
      >
        more..
      </a> */}
    </Panel>
  );
};
