import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Panel from '../common/panel/panel';
import './home.scss';

export const HomeReviews = () => (
  <Row>
    <Col>
      <Panel title="Reviews">
        <div className="text-center reviews">
          <br />
          <h4>appslikethese.com</h4>
          <p>
            <a
              href="https://www.appslikethese.com/caves-of-mars-app-review/"
              target="_blank"
              rel="noreferrer"
              className="nav-link"
            >
              Caves Of Mars App Review
            </a>
          </p>
          <br />
          <h4>freeappsforme.com</h4>
          <p>
            <a href="https://freeappsforme.com/caves-of-mars-app-review/" target="_blank" rel="noreferrer" className="nav-link">
              Caves Of Mars App Review
            </a>
          </p>
          <br />
          <h4>gameskeys.net</h4>
          <p>
            <a
              href="https://gameskeys.net/top-steam-games-to-tryout-in-september-2021/#CavesOfMars"
              target="_blank"
              rel="noreferrer"
              className="nav-link"
            >
              Top Steam Games To Tryout In September 2021
            </a>
          </p>
          <br />
          <h4>game1news.com</h4>
          <p>
            <a href="https://game1news.com/upcoming-game-caves-of-mars/" target="_blank" rel="noreferrer" className="nav-link">
              Upcoming Game: Caves of Mars
            </a>
          </p>
          <br />
          <p>More reviews coming soon...</p>
        </div>
      </Panel>
    </Col>
  </Row>
);
