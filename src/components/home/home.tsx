import React from 'react';
import { Container, Image } from 'react-bootstrap';
import { EmptyRow } from '../common/emptyRow';
import Layout from '../layout/layout';
import Seo from '../common/seo';
import { HomeAbout } from './home.about';
import { HomeTrailer } from './home.trailer';
import ImageGallery from '../common/imageGallery';
import Panel from '../common/panel/panel';
import { HomeReviews } from './home.reviews';
import DownloadPanel from '../common/downloadPanel';

import './home.scss';
import banner from '../../static/images/caves_of_mars_banner.jpg';

const date = new Date(2021, 8, 29);
const today = new Date();

export const Home = () => (
  <Layout>
    <Seo title="Home" />
    <Container fluid className="home">
      <div className="content">
        <Container>
          <div className="banner">
            <Image src={banner} fluid rounded />
            <div className="banner-text"></div>
          </div>

          <DownloadPanel />

          <EmptyRow />

          <HomeAbout />

          <EmptyRow />

          <HomeTrailer />

          <EmptyRow />

          <Panel title="Images">
            <ImageGallery />
          </Panel>

          <EmptyRow />

          <HomeReviews />
        </Container>
      </div>
    </Container>
  </Layout>
);
