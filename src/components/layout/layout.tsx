import React, { ReactNode } from 'react';
import { Footer } from '../footer/footer';
import Header from '../header/header';
// import { useStaticQuery, graphql } from 'gatsby';
import './layout.scss';

interface LayoutProps {
  children: ReactNode;
}

const Layout = ({ children }: LayoutProps) => (
  <>
    <Header />
    <main>{children}</main>
    <Footer />
  </>
);

export default Layout;
