import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';
import { Row, Col } from 'react-bootstrap';

const AboutObstacles = () => (
  <>
    <h2 className="text-center">Laser Beams</h2>
    <p>
      Beware of the laser beams which you will need to negotiate your way through as they switch on and off. Each laser beam will
      turn off and on as you approach it. To return you need to navigate the sequence of laser beams before returning.
    </p>

    <Row className="justify-content-center my-5">
      <Col sm={6}>
        <StaticImage
          className="key-card"
          src="../../images/screenshots/Screenshot 2021-07-19 at 12.00.15.jpg"
          alt="Laser Beams"
          placeholder="none"
        />
      </Col>
    </Row>

    <h2 className="text-center">Moving Platforms</h2>
    <p className="text-center">Navigate your way through moving platforms</p>

    <Row className="justify-content-center my-5">
      <Col sm={6}>
        <StaticImage
          className="key-card"
          src="../../images/screenshots/Screenshot 2021-07-19 at 11.55.39.jpg"
          alt="Moving Platforms"
          placeholder="none"
        />
      </Col>
    </Row>
  </>
);

export default AboutObstacles;
