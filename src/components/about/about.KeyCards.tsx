import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';
import { Row, Col } from 'react-bootstrap';
import './about.scss';

const AboutKeyCards = () => (
  <>
    <h2 className="text-center">Key Cards and Laser Doors</h2>
    <p className="mb-4">Find and collect 'Key Cards' to unlock ‘Laser Doors’ to gain access to each section of these caves.</p>

    <Row className="justify-content-center key-cards">
      <Col xs={8} md={6} className="d-flex justify-content-center">
        <StaticImage
          className="key-card"
          src="../../images/keycards_doors/SecurityCard_Blue00000.png"
          alt="Key Card Blue"
          placeholder="none"
        />
        <StaticImage
          className="key-card"
          src="../../images/keycards_doors/SecurityCard_Green00000.png"
          alt="Key Card Green"
          placeholder="none"
        />
        <StaticImage
          className="key-card"
          src="../../images/keycards_doors/SecurityCard_Purple00000.png"
          alt="Key Card Purple"
          placeholder="none"
        />
        <StaticImage
          className="key-card"
          src="../../images/keycards_doors/SecurityCard_Red00000.png"
          alt="Key Card Red"
          placeholder="none"
        />
      </Col>
    </Row>

    <Row className="justify-content-center mb-5">
      <Col xs={8} md={6}>
        <StaticImage
          src="../../images/keycards_doors/Screenshot-2021-07-17-at-14.54.32.jpg"
          alt="Laser Door"
          placeholder="none"
        />
      </Col>
    </Row>

    <h2 className="text-center">Map</h2>
    <p>
      To help there is a map of each cave system indicating the location of each Key Card and the laser doors they unlock. Open
      this by clicking on the map button. The caves get larger and more complex as you progress so you will need this map.
    </p>

    <Row className="justify-content-center my-5">
      <Col sm={6}>
        <StaticImage src="../../images/gameshots/Screenshot 2021-07-17 at 15.34.50.jpg" alt="Key Map" placeholder="none" />
      </Col>
    </Row>
  </>
);

export default AboutKeyCards;
