import React from 'react';

const AboutMission = () => (
  <div>
    <h2 className="text-center">Mission</h2>
    <p>
      Tharsis is a mountainous region on Mars where hundreds of caves have been discovered. These caves contain precious gems
      which are heavily guarded by enemies and alien life forms.
    </p>
    <p>
      Your mission is to explore these caves, destroy the enemies and collect as many gems as possible. Collecting gems will
      increase your health and gain points. You will gain extra bonus points the more gems you collect and enemies you destroy.
    </p>
    <p>
      There are 100 cave levels to complete, each one generated completely randomly so no two levels are ever the same. As you
      progress through the levels the caves will become larger, more complex and you will encounter more challenging enemies,
      aliens and obstacles.
    </p>
  </div>
);

export default AboutMission;
