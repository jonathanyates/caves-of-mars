import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';
import { Col, Row } from 'react-bootstrap';

const AboutEnemies = () => (
  <div className="my-5">
    <h2 className="text-center">Enemies</h2>
    <p>
      You will encounter many different enemies as you progress though the caves. Some guard their position and don’t move, whilst
      others form waves of enemy spacecraft which will follow you. The more powerful enemies will take serval hits to destroy.
    </p>

    <Row className="justify-content-center my-5">
      <Col sm={6}>
        <StaticImage src="../../images/enemies/enemies1.png" alt="Enemies" placeholder="none" />
      </Col>
    </Row>

    <h2 className="text-center">Enemy Bosses</h2>
    <p>
      Watch out for enemy boss spacecraft. Some of the Key Cards are in the possession of enemy bosses so you will need your
      weapons to destroy these and collect the Key Card.
    </p>

    <Row className="justify-content-center my-5">
      <Col sm={6}>
        <StaticImage src="../../images/enemies/enemy_bosses.png" alt="Enemies" placeholder="none" />
      </Col>
    </Row>

    <h2 className="text-center">Aliens</h2>
    <p>Watch out for Alien lifeforms. Some of these can move, jump or fly and throw lightning bolts.</p>

    <Row className="justify-content-center mb-5">
      <Col sm={6} className="d-flex justify-content-center">
        <StaticImage src="../../images/enemies/AlienRound_00000.png" alt="Enemies" placeholder="none" />
        <StaticImage src="../../images/enemies/EnemyNeck_00000.png" alt="Enemies" placeholder="none" />
        <StaticImage src="../../images/enemies/EnemyFlying_00006.png" alt="Enemies" placeholder="none" />
      </Col>
    </Row>
  </div>
);

export default AboutEnemies;
