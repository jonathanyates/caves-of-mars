import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';
import { Row, Col } from 'react-bootstrap';

const AboutFuel = () => (
  <>
    <h2 className="text-center">Fuel</h2>
    <p className="text-center">Collect fuel to boost your health.</p>

    <Row className="justify-content-center mt-3 mb-5">
      <Col className="d-flex justify-content-center">
        <StaticImage
          src="../../images/powerups/Fuel Tank_00000.png"
          alt="Enemies"
          placeholder="none"
          layout="constrained"
          width={200}
        />
      </Col>
    </Row>
  </>
);

export default AboutFuel;
