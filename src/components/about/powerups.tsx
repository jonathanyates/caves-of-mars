import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';

export interface PowerUp {
  img: string;
  alt: string;
  text: string;
  subText?: string;
  spacecraftImg?: any;
}

export const powerups: PowerUp[] = [
  {
    img: 'twin-blue-laser-powerup.png',
    alt: 'Twin Blue Laser',
    text: 'Twin Blue Laser',
    spacecraftImg: (
      <StaticImage className="key-card" src="../../images/player_spacecraft/IceShip.png" alt="KeyCard" placeholder="none" />
    ),
  },
  {
    img: 'fire-laser-powerup.png',
    alt: 'Fire Laser',
    text: 'Fire Laser',
    spacecraftImg: (
      <StaticImage className="key-card" src="../../images/player_spacecraft/FireShip.png" alt="KeyCard" placeholder="none" />
    ),
  },
  {
    img: 'twin-fire-laser-powerup.png',
    alt: 'Twin Fire Laser',
    text: 'Twin Fire Laser',
    spacecraftImg: (
      <StaticImage className="key-card" src="../../images/player_spacecraft/FireShip.png" alt="KeyCard" placeholder="none" />
    ),
  },
  {
    img: 'missile-powerup.png',
    alt: 'Missile',
    text: 'Missile',
    spacecraftImg: (
      <StaticImage className="key-card" src="../../images/player_spacecraft/MissileShip.png" alt="KeyCard" placeholder="none" />
    ),
  },
  {
    img: 'twin-missile-powerup.png',
    alt: 'Twin Missile',
    text: 'Twin Missile',
    spacecraftImg: (
      <StaticImage className="key-card" src="../../images/player_spacecraft/MissileShip.png" alt="KeyCard" placeholder="none" />
    ),
  },
  {
    img: 'green-bomb-powerup.png',
    alt: 'Green Bomb',
    text: 'Green Bomb',
    subText: 'Bouncing exploding bombs',
    spacecraftImg: (
      <StaticImage className="key-card" src="../../images/player_spacecraft/GreenShip.png" alt="KeyCard" placeholder="none" />
    ),
  },
  {
    img: 'black-hole-powerup.png',
    alt: 'Black Hole Bomb',
    text: 'Black Hole Bomb',
    subText: 'Destroys all enemies in range',
    spacecraftImg: (
      <StaticImage className="key-card" src="../../images/player_spacecraft/BlackHoleShip.png" alt="KeyCard" placeholder="none" />
    ),
  },
  {
    img: 'shield-powerup.png',
    alt: 'Shield',
    text: 'Shield',
    subText: 'Shields your spacecraft',
    spacecraftImg: (
      <StaticImage className="key-card" src="../../images/player_spacecraft/NormalShip.png" alt="KeyCard" placeholder="none" />
    ),
  },
];
