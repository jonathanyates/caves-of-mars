import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import { getImage, IGatsbyImageData, GatsbyImage } from 'gatsby-plugin-image';
import { Row, Col, Card } from 'react-bootstrap';
import { PowerUp, powerups } from './powerups';

const AboutPowerups = () => {
  const data: any = useStaticQuery(graphql`
    {
      allFile(filter: { relativeDirectory: { eq: "powerups" }, publicURL: { regex: "/powerup/" } }) {
        nodes {
          relativeDirectory
          publicURL
          name
          childImageSharp {
            gatsbyImageData(placeholder: BLURRED, formats: [AUTO, WEBP, AVIF])
          }
        }
      }
    }
  `);

  var nodes: any[] = data.allFile.nodes;

  return (
    <>
      <h2 className="text-center">Power Ups</h2>
      <p>
        Collect vessels containing Weapons and Shield Powerups. Collected powerups are displayed in the right-hand panel. Activate
        / deactivate these by clicking them or selecting keys 1-8. Each powerup with change the appearance of your player
        spacecraft.
      </p>
      <Row className="justify-content-center mb-5">
        <Col md={12} xl={10}>
          <div
            style={{
              display: 'grid',
              gridTemplateColumns: '25% 25% 25% 25%',
              gridGap: 0,
            }}
          >
            {powerups.map((powerup: PowerUp) => {
              var node = nodes.find((node) => node.publicURL.includes(powerup.img));
              var image = getImage(node) as IGatsbyImageData;
              return (
                <Card className="text-center">
                  <Card.Body>
                    <div className="powerup-ship">{powerup.spacecraftImg}</div>

                    <GatsbyImage className="powerup" image={image} alt={powerup.alt} />

                    <div className="text-center powerup-name">{powerup?.text}</div>
                    <div className="text-center d-none d-lg-block d-lg-none">{powerup?.subText}</div>
                  </Card.Body>
                </Card>
              );
            })}
          </div>
        </Col>
      </Row>
    </>
  );
};

export default AboutPowerups;
