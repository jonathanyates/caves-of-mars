import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';
import { Row, Col } from 'react-bootstrap';

const AboutControls = () => (
  <div className="my-5">
    <h2 className="text-center">Controls</h2>
    <p>Your spacecraft will automatically descend downwards until it hits the ground or reaches an obstacle.</p>
    <h3>PC</h3>
    <p>Control your spacecraft using your keyboard Arrow Keys or WASD keys. Fire using either Spacebar or Mouse Click.</p>
    <h3>Mobile</h3>
    <p>
      Control your spacecraft by tilting your device in the direction you wish to travel, or you can enable the on-screen
      controller in settings. Fire by tapping on the screen.
    </p>

    <Row className="justify-content-center my-3">
      <Col xs={12} md={12} lg={6}>
        <StaticImage className="key-card" src="../../images/Movement_Setting.png" alt="Moving Platforms" placeholder="none" />
      </Col>
    </Row>

    <Row className="justify-content-center mb-5">
      <Col className="d-flex justify-content-center">
        <StaticImage src="../../images/Controller.png" alt="Controller" placeholder="none" layout="constrained" width={200} />
      </Col>
    </Row>
  </div>
);

export default AboutControls;
