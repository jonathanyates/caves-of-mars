import React from 'react';
import { Home } from '../components/home/home';

const IndexPage = () => <Home />;

export default IndexPage;
