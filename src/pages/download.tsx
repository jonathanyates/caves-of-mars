import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import Seo from '../components/common/seo';
import Layout from '../components/layout/layout';
import DownloadPanel from '../components/common/downloadPanel';

import './page.scss';
import banner from '../static/images/logo_with_background.jpg';

const Download = () => (
  <Layout>
    <Seo title="Contact Us" />
    <Container fluid className="page">
      <div className="background">
        <Container className="content">
          <div className="banner">
            <Image src={banner} fluid rounded width="100%" />
          </div>
          <Row>
            <Col>
              <DownloadPanel />
            </Col>
          </Row>
        </Container>
      </div>
    </Container>
  </Layout>
);

export default Download;
