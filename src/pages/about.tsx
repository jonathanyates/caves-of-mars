import './page.scss';
import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import Panel from '../components/common/panel/panel';
import Seo from '../components/common/seo';
import Layout from '../components/layout/layout';
import AboutPowerups from '../components/about/about.powerups';
import AboutMission from '../components/about/about.mission';
import AboutEnemies from '../components/about/about.enemies';
import AboutFuel from '../components/about/about.fuel';
import AboutKeyCards from '../components/about/about.KeyCards';
import AboutObstacles from '../components/about/about.obstacles';
import AboutControls from '../components/about/about.controls';
import banner from '../static/images/logo_with_background.jpg';

const About = () => {
  return (
    <Layout>
      <Seo title="About" />
      <Container fluid className="page about">
        <div className="background">
          <Container className="content">
            <div className="banner">
              <Image src={banner} fluid rounded width="100%" />
            </div>
            <Row>
              <Col>
                <Panel title="About">
                  <Container>
                    <AboutMission />
                    <AboutEnemies />
                    <AboutFuel />
                    <AboutPowerups />
                    <AboutKeyCards />
                    <AboutObstacles />
                    <AboutControls />
                  </Container>
                </Panel>
              </Col>
            </Row>
          </Container>
        </div>
      </Container>
    </Layout>
  );
};

export default About;
