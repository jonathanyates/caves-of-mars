import React from 'react';
import { Container, Image } from 'react-bootstrap';
import Seo from '../components/common/seo';
import { HomeReviews } from '../components/home/home.reviews';
import Layout from '../components/layout/layout';
import './page.scss';
import './reviews.scss';
import banner from '../static/images/logo_with_background.jpg';

const Reviews = () => (
  <Layout>
    <Seo title="Reviews" />
    <Container fluid className="page">
      <div className="background">
        <Container className="content">
          <div className="banner">
            <Image src={banner} fluid rounded width="100%" />
          </div>
          <HomeReviews />
        </Container>
      </div>
    </Container>
  </Layout>
);

export default Reviews;
