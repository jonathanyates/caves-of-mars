import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import Panel from '../components/common/panel/panel';
import Seo from '../components/common/seo';
import SocialMedia from '../components/common/socialMedia';
import Layout from '../components/layout/layout';
import './page.scss';
import banner from '../static/images/logo_with_background.jpg';

const Contact = () => (
  <Layout>
    <Seo title="Contact Us" />
    <Container fluid className="page">
      <div className="background">
        <Container className="content">
          <div className="banner">
            <Image src={banner} fluid rounded width="100%" />
          </div>
          <Row>
            <Col>
              <Panel title="Contact Us">
                <div className="text-center">
                  <p>If you have any questions or just want to get in touch then we'd love to hear from you.</p>
                  <p>Please email us or contact us via one of our social media channels.</p>
                </div>
                <SocialMedia />
              </Panel>
            </Col>
          </Row>
        </Container>
      </div>
    </Container>
  </Layout>
);

export default Contact;
