import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import Panel from '../components/common/panel/panel';
import Seo from '../components/common/seo';
import Layout from '../components/layout/layout';
import ImageGallery from '../components/common/imageGallery';
import './page.scss';
import './presskit.scss';
import { socialMediaUrls } from '../components/common/socialMedia';
import { FaTwitter, FaFacebookSquare, FaInstagram, FaYoutube, FaEnvelope, FaSteam, FaGlobe } from 'react-icons/fa';
import { YouTube } from '../components/common/youTube';
import { StaticImage } from 'gatsby-plugin-image';
import { MediaIconLink } from '../components/common/iconLink';
import banner from '../static/images/logo_with_background.jpg';

const Presskit = () => (
  <Layout>
    <Seo title="Press Kit" />
    <Container fluid className="page">
      <div className="background">
        <Container className="content">
          <div className="banner">
            <Image src={banner} fluid rounded width="100%" />
          </div>
          <Row>
            <Col>
              <Panel title="Press Kit">
                <div className="presskit">
                  <h3>Description</h3>

                  <p>The Caves Of Mars is the new 2D action space shooter game set in the mysterious caves of the planet Mars.</p>
                  <p>
                    Navigate your spacecraft through these caves, destroy enemies and alien lifeforms and collect as many gems as
                    possible. Collecting gems will increase your health and gain points. You will gain extra bonus points the more
                    gems you collect and enemies you destroy.
                  </p>
                  <p>
                    There are 100 cave levels to complete, each one procedurally generated randomly so no two levels are ever the
                    same. As you progress through the levels the caves will become larger, more complex and you will encounter
                    more challenging enemies, aliens and obstacles.
                  </p>

                  <h3>Background</h3>
                  <p>
                    Caves Of Mars has been developed by Jonathan Yates, a software developer with over 30 years of software
                    development experience. Work began in March 2020 using Unity Game Engine and has been developed over an
                    18-month period. During this time Silkweb Games was formed and this is their first title.
                  </p>
                  <p>
                    The game is inspired by some 80’s classic 8-bit games such as ‘Caverns Of Mars’ and ‘Scramble’ but brings
                    these games right up to date with its own unique game play and modern graphics. The game is available to play
                    on PC, iOS and Android mobile devices.
                  </p>

                  <h3>Game Mission</h3>
                  <p>
                    Tharsis is a mountainous region on Mars where hundreds of caves have been discovered. These caves contain
                    precious gems which are heavily guarded by enemies and alien life forms.
                  </p>
                  <p>
                    Your mission is to explore these caves, destroy the enemies and collect as many gems as possible. Collecting
                    gems will increase your health and gain points. You will gain extra bonus points the more gems you collect and
                    enemies you destroy.
                  </p>
                  <p>
                    There are 100 cave levels to complete, each one generated completely randomly so no two levels are ever the
                    same. As you progress through the levels the caves will become larger, more complex and you will encounter
                    more challenging enemies, aliens and obstacles.
                  </p>

                  <h3>Features</h3>

                  <h5>Enemies</h5>
                  <p>
                    You will encounter many different enemies as you progress though the caves. Some guard their position and
                    don’t move, whilst others form waves of enemy spacecraft which will follow you. The more powerful enemies will
                    take serval hits to destroy.
                  </p>

                  <h5>Enemy Bosses</h5>
                  <p>
                    Watch out for enemy boss spacecraft. Some of the Key Cards are in the possession of enemy bosses so you will
                    need your weapons to destroy these and collect the Key Card.
                  </p>

                  <h5>Aliens</h5>
                  <p>Watch out for Alien lifeforms. Some of these can move, jump or fly and throw lightning bolts.</p>

                  <h5>Fuel</h5>
                  <p>Collect fuel to boost your health.</p>

                  <h5>Power Ups</h5>
                  <p>
                    Collect vessels containing Weapons and Shield Powerups. Collected powerups are displayed in the right-hand
                    panel. Activate / deactivate these by clicking them or selecting keys 1-8. Each powerup with change the
                    appearance of your player spacecraft.
                  </p>

                  <h5>Key Cards and Laser Doors</h5>
                  <p className="mb-4">
                    Find and collect 'Key Cards' to unlock ‘Laser Doors’ to gain access to each section of these caves.
                  </p>

                  <h5>Map</h5>
                  <p>
                    To help there is a map of each cave system indicating the location of each Key Card and the laser doors they
                    unlock. Open this by clicking on the map button. The caves get larger and more complex as you progress so you
                    will need this map.
                  </p>

                  <h5>Laser Beams</h5>
                  <p>
                    Beware of the laser beams which you will need to negotiate your way through as they switch on and off. Each
                    laser beam will turn off and on as you approach it. To return you need to navigate the sequence of laser beams
                    before returning.
                  </p>

                  <h5>Moving Platforms</h5>
                  <p>Navigate your way through moving platforms</p>

                  <h3 className="text-center mt-5 mb-3">Videos</h3>
                  <Row className="justify-content-center mb-4">
                    <Col xs={10}>
                      <YouTube embedId={socialMediaUrls.youTubeEmbedId} title="Caves Of Mars Trailer"></YouTube>
                    </Col>
                  </Row>

                  <Row className="justify-content-center mb-4">
                    <Col xs={10}>
                      <YouTube embedId={'6d0WSQGVwR8'} title="Caves Of Mars Short 30 second Trailer"></YouTube>
                    </Col>
                  </Row>

                  <h3 className="text-center mt-5 mb-3">Images</h3>
                  <ImageGallery />

                  <h3 className="text-center mt-5 mb-3">Logo</h3>
                  <div className="text-center">
                    <StaticImage
                      src="../images/caves_of_mars_logo_final.png"
                      alt="Play it on Steam"
                      height={300}
                      placeholder="none"
                    />
                  </div>

                  <h3 className="mt-4 mb-3">Website</h3>
                  <div>
                    <Row>
                      <Col className="d-flex flex-column">
                        <MediaIconLink
                          url={socialMediaUrls.website}
                          icon={FaGlobe}
                          text={socialMediaUrls.website}
                          smallText="www.cavesofmars.com"
                        />
                      </Col>
                    </Row>
                  </div>

                  <h3 className="mt-4 mb-3">Social Media</h3>
                  <div>
                    <Row>
                      <Col className="d-flex flex-column">
                        <MediaIconLink
                          url={socialMediaUrls.twitter}
                          icon={FaTwitter}
                          text={socialMediaUrls.twitter}
                          smallText="Twitter"
                        />
                        <MediaIconLink
                          url={socialMediaUrls.instagram}
                          icon={FaInstagram}
                          text={socialMediaUrls.instagram}
                          smallText="Instagram"
                        />
                        <MediaIconLink
                          url={socialMediaUrls.steam}
                          icon={FaSteam}
                          text={socialMediaUrls.steam}
                          smallText="Steam"
                        />
                        <MediaIconLink
                          url={socialMediaUrls.youTube}
                          icon={FaYoutube}
                          text={socialMediaUrls.youTube}
                          smallText="YouTube"
                        />
                        <MediaIconLink
                          url={socialMediaUrls.faceBook}
                          icon={FaFacebookSquare}
                          text={socialMediaUrls.faceBook}
                          smallText="Facebook"
                        />
                      </Col>
                    </Row>
                  </div>

                  <h3 className="mt-4 mb-3">Contact</h3>
                  <div>
                    <Row>
                      <Col className="d-flex flex-column">
                        <MediaIconLink
                          url={socialMediaUrls.email}
                          icon={FaEnvelope}
                          text={socialMediaUrls.email}
                          smallText="Email"
                        />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Panel>
            </Col>
          </Row>
        </Container>
      </div>
    </Container>
  </Layout>
);

export default Presskit;
