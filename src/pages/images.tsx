import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import Panel from '../components/common/panel/panel';
import ImageGallery from '../components/common/imageGallery';
import Seo from '../components/common/seo';
import Layout from '../components/layout/layout';
import './page.scss';
import banner from '../static/images/logo_with_background.jpg';

const Images = () => (
  <Layout>
    <Seo title="Images" />
    <Container fluid className="page">
      <div className="background">
        <Container className="content">
          <div className="banner">
            <Image src={banner} fluid rounded width="100%" />
          </div>
          <Row>
            <Col>
              <Panel title="Images">
                <ImageGallery />
              </Panel>
            </Col>
          </Row>
        </Container>
      </div>
    </Container>
  </Layout>
);

export default Images;
